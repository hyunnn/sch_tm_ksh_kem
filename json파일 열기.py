import json
fileName = input("열 파일 이름 : ")
try:
    with open(fileName + ".json",'r',encoding = "utf-8") as f:
        jsonFile = json.load(f)
        data = jsonFile['data']
        for i in data:
            print("제목 :" ,i['title'])
            print("내용 :" ,i['content'])
except Exception as ex:
    print(ex)
    print('파일이 존재하지 않거나 열 수 없습니다')
