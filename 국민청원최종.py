from selenium import webdriver
import time
import json
search = input("검색할 결과 : ")
filename = input("저장할 파일명 : ")
save_count = int(input("가져올 갯수 , -1은 전체 : "))
total = 0


dr = webdriver.Chrome('chromedriver')
dr.get('https://www1.president.go.kr/search')

dr.find_element_by_xpath('''//*[@id="contents"]/div[2]/div/ul/li[5]/a''').click()
dr.find_element_by_xpath('''//*[@id="query"]''').send_keys(search)
dr.find_element_by_xpath('''//*[@id="login"]/li[2]/input''').click()

count = dr.find_element_by_xpath('''//*[@id="contents"]/div[2]/div/div[1]/span[2]''').text
count = count.replace("(","")
count= count.replace(")","")
count = count.replace(",","")
count = count.replace("건","")
count = int(count)
count = count//70
jsonFile = {"data":[]}

try:

    for c in range(count+1):
        for button in range(1,8):
            dr.find_element_by_xpath('''//*[@id="contents"]/div[2]/div/div[3]/div/div[2]/a[%d]'''%button).click()
            
            time.sleep(1)
            for i in range(2,12):
                path = '''//*[@id="contents"]/div[2]/div/div[2]/div[%d]/a'''%i #제목
                link = dr.find_element_by_xpath(path).get_attribute("href")
                dr.get(link)
                title = dr.find_element_by_xpath('''//*[@id="cont_view"]/div[2]/div[1]/div/div[1]/div/h3''').text
                
                #print(title)
                content = dr.find_element_by_xpath('''//*[@id="cont_view"]/div[2]/div[1]/div/div[1]/div/div[4]/div[2]''').text
                content = content.replace("\"","`")

                jsonFile['data'].append({"title":title,"content":content})
                total += 1
                if(save_count != -1):
                    assert total < save_count
                dr.back()

        dr.find_element_by_xpath('''//*[@id="contents"]/div[2]/div/div[3]/div/div[3]/a''').click()
        
except Exception as ex:
    print(ex)
finally:
    with open(filename+".json",'w',encoding = 'utf-8') as outfile:
        json.dump(jsonFile,outfile,ensure_ascii=False)

